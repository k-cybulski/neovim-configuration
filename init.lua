vim.notify("Hello!")

-- Settings
require("user.settings")

-- Keybindings
require("user.keymaps")

-- Autocommands
require("user.auto")

-- Plugins
require("user.plugins")

-- LSP
require("user.lsp")

-- Debugging
require("user.debug")

-- Final custom bits
require("user.custom")
