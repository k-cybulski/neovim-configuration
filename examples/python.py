from dataclasses import dataclass
from models import Header, Data

@dataclass
class Payload(frozen=True):
    header: Header
    data: Data

