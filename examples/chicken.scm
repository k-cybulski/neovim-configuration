; Code is data i.e. we can "symbolize" any expression or statement
; These even require correct syntax to work, eagerly

'(define (g x) (let ((y 1)) (+ x y)))

; Also, this magical quote is really (quote <EXPR>)

(quote (define (g x) (let ((y 1)) (+ x y))))

'(lambda (x) (+ x 2))

