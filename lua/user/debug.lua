-- python
local dap = require('dap')
dap.adapters.python = {
  type = 'executable';
  -- TODO: find cleaner way to have debugpy package other than a virtualenv in
  -- this weird location
  command = vim.fn.expand('~/.local/share/debugpy-virtualenv/bin/python');
  args = { '-m', 'debugpy.adapter' };
}


dap.configurations.python = {
  {
    type = 'python';
    request = 'launch';
    name = "Launch file";
    program = "${file}";
    pythonPath = function()
      return 'python'
    end;
  },
}

vim.keymap.set('n', '<Leader>dR', function() require('dap').continue() end)
vim.keymap.set('n', '<Leader>dC', function() require('dap').close() end)
vim.keymap.set('n', '<Leader>do', function() require('dap').step_over() end)
vim.keymap.set('n', '<Leader>di', function() require('dap').step_into() end)
vim.keymap.set('n', '<Leader>dO', function() require('dap').step_out() end)
vim.keymap.set('n', '<Leader>b', function() require('dap').toggle_breakpoint() end)
vim.keymap.set('n', '<Leader>B', function() require('dap').set_breakpoint() end)
vim.keymap.set('n', '<Leader>dr', function() require('dap').repl.open() end)
vim.keymap.set('n', '<Leader>dl', function() require('dap').run_last() end)
vim.keymap.set({'n', 'v'}, '<Leader>dh', function()
  require('dap.ui.widgets').hover()
end)
vim.keymap.set({'n', 'v'}, '<Leader>dp', function()
  require('dap.ui.widgets').preview()
end)
vim.keymap.set('n', '<Leader>df', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.frames)
end)
vim.keymap.set('n', '<Leader>ds', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.scopes)
end)


-- Configure hints
local wk = require("which-key")
wk.register({
    d = {
        name = 'debugger',
        R = 'continue debugger',
        r = 'open debug console',
        C = 'close debugger',
        o = 'step over',
        O = 'step out',
        i = 'step into',
        l = 'rerun last configuration',
    },
    b = 'toggle debugger breakpoint';
    B = 'set debugger breakpoint',
}, { prefix = "<leader>" })
