-- This file includes slightly more elaborate functions that *could* one day grow to be plugins
-- For quick experimentation, run :luafile %

-- Copy lines of file with path and line numbers
function get_visual_line_range()
    -- Returns the first and last line selected in visual mode
    local _, firstline, _, _ = unpack(vim.fn.getpos("'<"))
    local _, lastline, _, _ = unpack(vim.fn.getpos("'>"))
    return firstline, lastline
end

function get_visual_lines_with_numbers()
    -- Loads lines selected in visual mode, and appends "<line number>:" at
    -- the beginning of each line
    local firstline, lastline = get_visual_line_range()
    local linearr = vim.api.nvim_buf_get_lines(0, firstline - 1, lastline, false)
    for idx = 1, lastline - firstline + 1 do
        local linenum = idx + firstline - 1
        -- assuming no file will have more than 4 digit line numbers...
        linearr[idx] = string.format("%4d: ", linenum) .. linearr[idx]
    end
    return linearr
end

function copy_visual_md_snippet()
    -- Copies a reference to the code snippet with a file name, line numbers,
    -- and the code bracketed by ```
    
    -- leave visual mode, which is necessary to get correct locations of
    -- current visual selection. See:
    -- - discussion about how visual selection is broken https://github.com/neovim/neovim/pull/13896
    -- - how to exit it https://github.com/neovim/neovim/issues/17735
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Esc>', false, true, true), 'nx', false)
    local path =  vim.fn.expand('%:p')
    local linearr = get_visual_lines_with_numbers()
    local out_str = "```\n"
    out_str = out_str .. path .. ":"
    for idx = 1, table.getn(linearr) do
        out_str = out_str .. "\n" .. linearr[idx]
    end
    out_str = out_str .. "\n```"
    vim.call('setreg', '+', out_str)
    print((table.getn(linearr)) .. " lines yanked in markdown mode")
end

vim.keymap.set('v', '<C-y>', copy_visual_md_snippet)

