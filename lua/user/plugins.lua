require("paq") {
    "savq/paq-nvim"; -- package manager
    "tpope/vim-commentary"; -- commenting between languages, e.g. type gcc
    "tpope/vim-surround"; -- changing delimiters, e.g. type inside quoted text: cs"'
    "preservim/nerdtree"; -- nicer file browser
    -- git
    "tpope/vim-fugitive"; -- helper git commands, e.g. :Git
    "lewis6991/gitsigns.nvim"; -- git diff markers
    -- handling programming languages
    "neovim/nvim-lspconfig"; -- general language servers
    "nvim-treesitter/nvim-treesitter"; -- code syntax tree handling
    "ggandor/leap.nvim"; -- jumping by letter pairs
    "p00f/nvim-ts-rainbow"; -- rainbow coloured matching parentheses
    "ActivityWatch/aw-watcher-vim"; -- ActivityWatch watcher for vim
    -- latex
    "lervag/vimtex";
    -- snippets
    "sirver/ultisnips";
    -- completion
    "hrsh7th/nvim-cmp";
    "hrsh7th/cmp-buffer";
    "hrsh7th/cmp-path";
    "hrsh7th/cmp-nvim-lsp";
    "quangnguyen30192/cmp-nvim-ultisnips"; -- snippets in completion
    -- linting
    "dense-analysis/ale";
    -- fancy status line, helpful for linting
    "vim-airline/vim-airline";
    "vim-airline/vim-airline-themes";
    -- fuzzy finding
    "junegunn/fzf.vim";
    -- debugging
    "mfussenegger/nvim-dap";
    -- key hints
    "folke/which-key.nvim";
}

-- vimtex
vim.g.tex_flavor = 'latex'
vim.g.vimtex_view_method = 'zathura'
vim.g.vimtex_quickfix_mode = 0
vim.o.conceallevel = 1
vim.g.tex_conceal = 'abdmg'
vim.g.vimtex_view_general_viewer='usr/bin/zathura'
-- Clean all non-pdf compiler outputs
vim.api.nvim_command([[
augroup autocom
     autocmd VimLeave *.tex :VimtexClean
augroup END
]])
vim.g.vimtex_compiler_latexmk = {
    build_dir = '';
    callback = 1,
    continuous = 1,
    executable = 'latexmk',
    hooks = {},
    options = {
        '-verbose',
        '-file-line-error',
        '-synctex=1',
        '-interaction=nonstopmode',
    },
}


-- UltiSnips
vim.g.UltiSnipsExpandTrigger = '<tab>'
vim.g.UltiSnipsJumpForwardTrigger = '<tab>'
vim.g.UltiSnipsJumpBackwardTrigger = '<s-tab>'

vim.cmd [[ let g:UltiSnipsSnippetDirectories=[stdpath("config") . '/UltiSnips'] ]]


-- Git signs
require('gitsigns').setup {
    signs = {
      add = { text = '+' },
      change = { text = '~' },
      delete = { text = '_' },
      topdelete = { text = '‾' },
      changedelete = { text = '~' },
    },
    signcolumn = false,
    numhl      = true,
}

-- Leap
require("leap").add_default_mappings()
-- removing leap's mappings on `x` in visual mode to retain `x` for deleting
-- text, as mentioned in the FAQ (I am indeed too used to using `x`)
-- https://github.com/ggandor/leap.nvim#faq
vim.keymap.del({'x', 'o'}, 'x')
vim.keymap.del({'x', 'o'}, 'X')

-- Treesitter
require("nvim-treesitter.configs").setup {
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "<cr>",
            node_incremental = "<cr>",
            node_decremental = "<bs>"
        },
    },
    highlight = {
        enable = true,
        disable = {"latex"}
    },
    indent = {
        enable = true,
        disable = {"python"}
    },
    rainbow = {
        enable = true,
        disable = {"lua", "python", "markdown", "bash"},
        extended_mode = false,
        max_file_lines = 10000,
    },
    ensure_installed = {
        "lua",
        "python",
        "markdown",
        "bash",
        "json",
        "scheme",
        "latex"
    },
}


-- NERDTree
vim.cmd [[ let NERDTreeCustomOpenArgs={'file': {'reuse':'currenttab', 'where':'p', 'keepopen':1, 'stay':1}} ]]

-- ALE
-- disable linting on all changes
vim.g.ale_lint_on_text_changed = 'never'
vim.g.ale_lint_on_insert_leave = 0

-- set/unset linters for specific languages
-- TODO: make languagetool ignore codeblocks for markdown
vim.g.ale_linters_ignore = {
  markdown = {'languagetool'},
  tex = {'alex', 'chktex', 'cspell', 'lacheck', 'proselint', 'redpen', 'texlab', 'textlint', 'vale', 'writegood'} -- disable linting for tex to save energy during lectures
}
vim.g.ale_linters = {python = {"ruff"}}

-- always remove trailing whitespace
vim.g.ale_fixers = {["*"] = {"remove_trailing_lines", "trim_whitespace"}}
vim.g.ale_fix_on_save = 1

-- Make the line length warnings pop up only if the lines are *really* long
vim.g.ale_python_flake8_options = "--max-doc-length 100 --max-line-length 100"

-- enable fixing with black and ruff only in projects that use them whether or
-- not a project uses black depends on whether or not the first
-- .envrc file up the directory tree contains an "export NVIM_USE_FIXER=1"
-- TODO: Find less hacky way to find this...

-- Helper command for conditionally enabling fixers
function envrc_match_pattern(start_directory, pattern)
    -- Goes up the directory tree from start_directory, and returns true if the
    -- first .envrc file found contains a line with pattern
    local parent_directory = start_directory
    local found_pattern = false
    while (parent_directory ~= "")
        -- Go up the directory tree.
        -- If .envrc is in the directory, to line by line to see if there is
        -- "export NVIM_USE_BLACK=1". If so, enable black fixer.
        do
        local envrc_path = parent_directory .. "/.envrc"
        local envrc_file = io.open(envrc_path, "r")
        if (envrc_file ~= nil)
        then
            for line in envrc_file:lines() do
                if string.find(line, pattern) then
                    found_pattern = true
                    break
                end
            end
            envrc_file:close()
            break
        end
        local path_sep = string.find(parent_directory, "/[^/]*$")
        parent_directory = parent_directory:sub(1, path_sep - 1)
    end
    return found_pattern
end

function table.shallow_copy(t)
    -- Helpful function for extending tables
    -- from https://stackoverflow.com/a/641993
    local t2 = {}
    for k,v in pairs(t) do
        t2[k] = v
    end
    return t2
end

-- Use black and ruff on python files if fixers enabled in given project
vim.api.nvim_create_autocmd(
    { "BufRead", "BufNewFile" },
    { pattern = { "*.py" }, callback = function()
        local parent_directory = vim.fn.expand('%:p:h')
        local found_use_fixer = envrc_match_pattern(parent_directory, "export NVIM_USE_FIXER *= *1")
        if (found_use_fixer) then
            -- To set fixers for a specific file, we need to set the dictionary vim.b.ale_fixers.
            -- vim.b.ale_fixers takes precedence over the global
            -- vim.g.ale_fixers, so below we copy the contents of
            -- vim.g.ale_fixers into a temporary dictionary, add the desired
            -- fixers to this temporary dictionary, and set vim.b.ale_fixers to
            -- be this dictionary.

            local b_ale_fixers = table.shallow_copy(vim.g.ale_fixers)
            b_ale_fixers['python'] = {"black", "ruff"}
            vim.b.ale_fixers = b_ale_fixers
            -- A note on the above implementation: We cannot do
            -- > vim.b.ale_fixers = table.shallow_copy(vim.g.ale_fixers)
            -- > vim.b.ale_fixers['python'] = {"black", "ruff"}
            -- because the lua-vimscript bridge does not allow for setting
            -- individual values of a table.
            -- From https://neovim.io/doc/user/lua.html#lua-vimscript
            --  > Note that setting dictionary fields directly will not write
            --  > them back into Nvim. This is because the index into the
            --  > namespace simply returns a copy. Instead the whole dictionary
            --  > must be written as one. This can be achieved by creating a
            --  > short-lived temporary.
            --
            -- Also, for this reason, using table.shallow_copy is an overkill
            -- in the first place. But it *feels* like a bug not to do it, so
            -- I'm keeping it. For now...
        end
    end
})

-- Airline
vim.g.airline_theme="minimalist"

-- Airline + ALE
vim.cmd [[ let g:airline#extensions#ale#enabled = 1 ]]
