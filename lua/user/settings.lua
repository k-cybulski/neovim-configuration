-- Search text as UTF-8,
-- ignore casing unless
-- it's the first letter.
vim.opt.encoding = "utf8"
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Clipboard
vim.cmd [[set clipboard+=unnamedplus]]

-- Set up a colorscheme
vim.o.termguicolors = true
vim.cmd [[colorscheme lunaperche]]

-- Wrap lines
vim.opt.wrap = true

-- Tabs are 4 spaces, and are
-- converted automatically,
-- and indents carry on
-- TODO: Make this depend on file
local tab_to_spaces = 4
vim.opt.tabstop = tab_to_spaces
vim.opt.shiftwidth = tab_to_spaces
vim.opt.expandtab = true
vim.opt.autoindent = true

-- File specific tab rules
vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
    desc = "Markdown specific tab rules",
    pattern = {"*.md"},
    callback = function()
        local tab_to_spaces = 2
        vim.opt.tabstop = tab_to_spaces
        vim.opt.shiftwidth = tab_to_spaces
    end,
})

vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
    desc = "Markdown specific tab rules",
    pattern = {"*.lua"},
    callback = function()
        local tab_to_spaces = 2
        vim.opt.tabstop = tab_to_spaces
        vim.opt.shiftwidth = tab_to_spaces
    end,
})


-- Show title as filename
vim.opt.title = true

-- No swap and no backup
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.writebackup = false

-- File explorer using built-in :Lexplore
-- TODO: close buffer after opening file
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 30

-- Grep settings (requires rg)
vim.opt.grepprg = "rg --vimgrep --follow"
vim.opt.errorformat:append("%f:%l:%c%p%m")

-- For faster responses, but make sure swaps are off
-- TODO: Benchmark disk writes, tune this better
vim.o.updatetime = 300

-- Number column
vim.wo.signcolumn = "number"
vim.opt.number = true
vim.opt.numberwidth = 1

-- Completion settings
vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

-- Highlight trailing whitespace
vim.cmd [[ match Todo /\s\+$/ ]]
vim.cmd [[ hi Todo guibg=white ]]

-- Spellcheck
vim.api.nvim_create_autocmd(
    { "BufRead", "BufNewFile" },
    { pattern = { "*.txt", "*.md", "*.tex" }, command = "setlocal spell spelllang=en_gb" }
)

-- Saving undo/changes between sessions
vim.opt.undofile = true
vim.cmd [[  let &undodir=stdpath("data") . '/undo' ]]

-- Python virtualenv
vim.cmd [[  let g:python3_host_prog=stdpath("data") . '/virtualenv/bin/python3' ]]
